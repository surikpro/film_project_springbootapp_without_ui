package com.company.aspects;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

import java.time.LocalTime;

@Slf4j
@Component
@Aspect
public class TimeAspect {

    @Before(value = "execution(* com.company.controllers.*.*(..))")
    public void beforeControllerMethodExecution() {
        log.info("Current time before method execution - {}", LocalTime.now().toString());
    }

    @AfterReturning(value = "execution(* com.company.controllers.*.*(..))", returning = "pageName")
    public void afterControllerMethodExecution(String pageName) {
        log.info("Current time for page {} after method execution - {}", pageName, LocalTime.now().toString());
    }
}

package com.company.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**

 */
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
@Entity
public class FileInfo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String storageFileName;

    private String originalFileName;

    private String mimeType;

    private Long size;

    private String description;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "film_id")

    private Film film;

    public Film getFilm() {
        return film;
    }

    public void setFilm(Film film) {
        this.film = film;
    }
}

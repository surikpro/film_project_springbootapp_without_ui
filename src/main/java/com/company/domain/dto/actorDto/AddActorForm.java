package com.company.domain.dto.actorDto;

import lombok.Data;

/**

 */
@Data
public class AddActorForm {
    private String firstName;
    private String lastName;
}

package com.company.domain.dto.actorDto;

import lombok.Data;

@Data
public class UpdateActorDto {
    private String actorFirstName;
    private String actorLastName;

}

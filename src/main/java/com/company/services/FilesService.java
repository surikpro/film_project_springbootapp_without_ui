package com.company.services;

import com.company.domain.entity.FileInfo;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**

 */
public interface FilesService {
    void saveFile(MultipartFile file, String description);

    Object addFileToResponse(String fileName, HttpServletResponse response);

    List<FileInfo> getAllById(Long filmId);

    void saveFileByFilmId(MultipartFile file, String description, Long filmId);

//    void deleteFileInfoById(Long fileId);

    void setNullToFilmIdOfFile(Long fileId);
}

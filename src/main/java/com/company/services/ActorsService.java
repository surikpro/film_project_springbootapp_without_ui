package com.company.services;

import com.company.domain.dto.actorDto.ActorDto;
import com.company.domain.dto.actorDto.AddActorForm;
import com.company.domain.dto.actorDto.UpdateActorDto;
import com.company.domain.entity.Actor;

import java.util.List;

/**

 */
public interface ActorsService {
    void addActor(AddActorForm form);
    List<ActorDto> getAllActors();
    void deleteActorById(Long actorId);
    void updateActor(Long actorId, UpdateActorDto updatedActor);
    Actor getActor(Long actorId);
}

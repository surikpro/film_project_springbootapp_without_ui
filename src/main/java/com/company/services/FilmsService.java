package com.company.services;

import com.company.domain.dto.actorDto.ActorDto;
import com.company.domain.dto.filmDto.FilmDto;

import java.util.List;

public interface FilmsService {
    void addFilm(FilmDto film);
    List<FilmDto> getAllFilms();
    List<ActorDto> getActorsByFilm(Long filmId);
    void addActorToFilm(Long filmId, ActorDto actor);
    List<ActorDto> getActorsWithoutFilm();

    FilmDto getFilmById(Long filmId);
}

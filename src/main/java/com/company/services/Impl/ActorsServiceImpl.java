package com.company.services.Impl;

import com.company.domain.dto.actorDto.ActorDto;
import com.company.domain.dto.actorDto.AddActorForm;
import com.company.domain.dto.actorDto.UpdateActorDto;
import com.company.domain.entity.Actor;
import com.company.repositories.ActorsRepository;
import com.company.services.ActorsService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

import static com.company.domain.dto.actorDto.ActorDto.from;


/**
 * by Aydar Zakirov
 * since 14.02.2022
 * */
@Service
@RequiredArgsConstructor
public class ActorsServiceImpl implements ActorsService {

    private final ActorsRepository actorsRepository;

    @Override
    public List<ActorDto> getAllActors() {
        return from(actorsRepository.findAll());
    }

    @Override
    public void addActor(AddActorForm form) {
        Actor actor = Actor.builder()
                .firstName(form.getFirstName())
                .lastName(form.getLastName())
                .build();

        actorsRepository.save(actor);
    }

    @Override
    public void deleteActorById(Long actorId) {
        Optional<Actor> actor = actorsRepository.findById(actorId);
        actor.get().setFilm(null);
        actorsRepository.deleteById(actorId);
    }

    @Override
    public void updateActor(Long actorId, UpdateActorDto updateActorDto) {
        Actor actorFromDB = actorsRepository.findActorById(actorId);
        actorFromDB.setFirstName(updateActorDto.getActorFirstName());
        actorFromDB.setLastName(updateActorDto.getActorLastName());
        actorsRepository.save(actorFromDB);
    }

    @Override
    public Actor getActor(Long actorId) {
        Actor actor = Optional.of(actorsRepository.findActorById(actorId)).get();
        return actorsRepository.findActorById(actorId);
    }

}

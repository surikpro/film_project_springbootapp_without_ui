function addFilm(name, csrfTokenValue) {
    let body = {
        "name": name,
        "_csrf" : "${_csrf.token}"
    };
    $.ajax({
        type: "POST",
        url: "/films",
        headers: {
            'X-CSRF-TOKEN' : csrfTokenValue
        },
        data: JSON.stringify(body),
        success: response => renderFilmTable(response),
        error: function () {
            alert("Ошибка")
        },
        dataType: "json",
        contentType: "application/json"
    });
}
function renderFilmTable(response) {
    let html = '<tr>' +
        '<th>Id</th>' +
        '<th>Name</th>' +
        '</tr>';

    for (let i = 0; i < response.length; i++) {
        html += '<tr>';
        html += '<td>' + response[i]['id'] + '</td>';
        html += '<td>' + response[i]['name'] + '</td>';
        html += '</tr>'
    }

    $('#film_table').empty().append(html);
}